# YBYRÁ

YBYRÁ: Copyright (C) 2014-2023, by Denis Jacob Machado. Current version: 20230912.

## AVAILABILITY

YBYRÁ comes with ABSOLUTELY NO WARRANTY!

YBYRÁ is available at www.ib.usp.br/grant/anfibios/researchSoftware.html.

The programs may be freely used, modified, and shared under the GNU General Public License version 3.0 (GPL-3.0, http://opensource.org/licenses/GPL-3.0).

See LICENSE.txt for complete information about terms and conditions.

YBYRÁ is also available at GitLab: https://gitlab.com/MachadoDJ/ybyra.
Also, check the wiki page that are available at https://gitlab.com/MachadoDJ/ybyra/wikis/home/.

## CONTACT

- Author: Denis Jacob Machado, Ph.D.
- Homepage: https://phyloinformatics.com
- Email: dmachado@charlotte.edu

## SUMMARY

The YBYRÁ project integrates software solutions for data analysis in phylogenetics. It comprises tools for: (1) topological distance calculation based on the number of shared splits or clades, (2) sensitivity analysis and automatic generation of sensitivity plots ("Navajo rugs") and (3) categorization of synapomorphies (using TNT) and automatic generation of diagnostic character state plots. YBYRÁ also provides (4) a framework to facilitate the search for potential rogue taxa based on how much they affect average matching split distances (using MSdist).

## DEPENDENCIES

YBYRÁ is written in Python, version 3.

Although YBYRÁ may be executed as an stand-alone application, some functionalities require easy-to-install Python libraries.

If you require Python libraries that are not yet installed in your system, YBYRÁ will exit with an error telling you what is missing.

These are the libraries that you might need:
- **Tkinter**: to run `easyConfiguration.py`
- **svgwrite**: to print SVG images
- **Biopython**: to root and/or prune trees
-
You will need a working installation of MSdist to search for potential wildcard taxa using `ybyra_sa.py` (example 4, see QUICK TUTORIAL bellow).

MSdist is available at http://kaims.pl/~dambo/msdist/.
You will need a working installation of TNT to run `ybyra_apo.py` (example 5, see QUICK TUTORIAL bellow).

TNT is available at http://www.lillo.org.ar/phylogeny/tnt/.
Windows doesn't provide a Bourne-like shell. Windows users will need to install a software to provide Unix-like environment under Windows (e.g., Cygwin, available at http://cygwin.com/) or other Unix-like subsystems (e.g., MinGW, available at http://www.mingw.org/).

## BEFORE START USING YBYRÁ

YBYRÁ comes with `ybyra_sa.py` and `ybyra_apo.py`, among other programs.

Trough `ybyra_sa.py` the user can compare different topologies, calculate topological distances, perform automated sensitivity analysis and search for potential unstable ("wildcard" or "rogue") taxa in a three step procedure using MSdist. The file configuration.txt is a commented configuration file for `ybyra_sa.py`, please take a look at this file. Also, the user can generate configuration files for ybyra_sa.py using a graphic interface by running `easyConfiguration.py`.

The program `ybyra_apo.py` uses TNT to categorize ambiguous and unambiguous (i.e., optimization dependent) synapomorphies in all trees. Unambiguous synapomorphies are latter categorized in unique and un-reversed (non-homoplastic), unique (homoplastic, a private stated that is not shared by all terminals derived from the corresponding node), and non-unique (an homoplastic non-private state).

See bellow a quick tutorial on how to use YBYRÁ.

## QUICK TUTORIAL

A quick tutorial and other resources are available in [YBYRÁ's Wiki page on GitLab](https://gitlab.com/MachadoDJ/ybyra/-/wikis/Tutorial).

## INPUT FILES

### Input files for `ybyra_sa.py`

The program `ybyra_sa.py` takes a configuration file and reads one or more tree files in NEWICK format.

YBYRÁ will read most tree files in NEWICK format, even if the names of the terminals are large or if the trees have special characters such as spaces, tabs and line breakers. However, we strongly suggest users to use only 0-9, a-z, A-Z, and _ for file and terminal names.

See `configuration.txt` that comes with YBYRÁ for details on how to prepare configuration and tree files.

The user may find our GUI (`easyConfiguration.py`) useful to prepare configuration files.

### Input files for `ybyra_apo.py`

The program `ybyra_apo.py` will take a tree file and a matrix file.

Tree files must be in TREAD format (see example.tre that comes with YBYRÁ). Tree blocks in TREAD format can be exported using Mesquite (formats "NONA, Hennig86, PiWe, Winclada" or "TNT"; Mesquite is available at http://www.mesquiteproject.org/).

Matrix files must be in a simplified NEXUS format containing a single DATA block (see example.nex that comes with YBYRÁ).

Terminal names in the matrix must match the terminal names in each tree. The order does not matter. We strongly advice the user against using long strings of characters as terminal names. We also advise against using spaces and/or special characters of any kind.

For more details, see [YBYRÁ's Wiki page on GitLab](https://gitlab.com/MachadoDJ/ybyra/-/wikis/).

## DESCRIPTION OF OUTPUT FILES

The description of YBYRÁ's output files and other resources are available in [YBYRÁ's Wiki page on GitLab](https://gitlab.com/MachadoDJ/ybyra/-/wikis/Output).

## CITE

Machado, D. J. (2015) YBYRÁ facilitates comparison of large phylogenetic trees. *BMC Bioinformatics*, 16, 204. doi: [10.1186/s12859-015-0642-9](https://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-015-0642-9).
