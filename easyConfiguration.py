#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# easyConfiguration.py

# YBYRÁ - Copyright (C) 2014 - Denis Jacob Machado
# GNU General Public License version 3.0

# Denis Jacob Machado
# denisjacobmachado@gmail.com
# www.ib.usp.br/grant/anfibios/researchSoftware.html

COPYRIGTH="""
easyConfiguration.py is part of the YBYRÁ project

version 20150824

For more information on how to fill each of the GUI's
fields, please look into the configutration.txt and
README.txt files that comes with YBYRÁ.

YBYRÁ - Copyright (C) 2014 - Denis Jacob Machado

    YBYRÁ comes with ABSOLUTELY NO WARRANTY!
    These are free software, and you are welcome to
    redistribute them under certain conditions
    For additional information, please visit:
    http://opensource.org/licenses/GPL-3.0

    YBYRÁ is available at
    www.ib.usp.br/grant/anfibios/researchSoftware.html
"""
print(COPYRIGTH)

# Import modules and libraries
import os,re
try:
	import tkinter as tk # This line imports the Tkinter module into your program's namespace, but renames it as tk
except ImportError:
	print(">ERROR: the TKinter package is not installed.\nSee <https://wiki.python.org/moin/TkInter>.")
	exit()
else:
	from tkinter import filedialog, messagebox
#	import filedialog,messagebox
#	from filedialog import askopenfilename

# Start GUI configuration and functions
class Application(tk.Frame): # The application class is inherit from Tkinter's Frame class
	def __init__(self,root):
		tk.Frame.__init__(self,root) # Calls the constructor for the parent class, Frame
		self.grid(sticky=tk.N+tk.S+tk.E+tk.W) # The argument makes the button expand to fill its cell of the grid
		self.createWidgets()
#		Menu
		menubar=tk.Menu(self)
		fileMenu=tk.Menu(self)
		helpMenu=tk.Menu()
		menubar.add_cascade(label="File",menu=fileMenu)
		fileMenu.add_command(label="Open tree file(s)",command=self.openFiles)
		fileMenu.add_command(label="Open configuration file",command=self.configurationFile)
		fileMenu.add_command(label="Exit",command=self.quit)
		menubar.add_cascade(label="Help",menu=helpMenu)
		helpMenu.add_command(label="How to use",command=self.manual)
		helpMenu.add_command(label="How to cite",command=self.cite)
		helpMenu.add_command(label="Copyright",command=self.copyrigth)
		root.configure(menu=menubar)

	def openFiles(self):self.files=filedialog.askopenfilenames(defaultextension="tree",title="Select the tree file(s)",initialdir=os.getcwd(),filetypes=[('all files','.*'),('text files','.txt'),('text files','.trees'),('text files','.tree'),('text files','.tre')],initialfile="trees.tre")

	def configurationFile(self):
#		Get configuration from file
		self.configurationFile=filedialog.askopenfilenames(defaultextension="conf",title="Select the configuration file(s)",initialdir=os.getcwd(),filetypes=[('all files','.*'),('text files','.txt'),('text files','.txt')],initialfile="configuration.txt")
		self.configurationFile=self.configurationFile[0]
		with open (self.configurationFile,'r') as self.configurationFile:
			configurationText=''
			for line in self.configurationFile:
				line=line.strip()
				if not (line.startswith('#') or line.startswith('%')):
					if line.rstrip():
						line=re.sub('\s*#.*|\s*%.*','',line)
						configurationText+=line+'\n'
		selectedText=re.compile('(>.*=)').findall(configurationText)
		selectedText+=re.compile('(<\s*begin\s+\w+)',re.IGNORECASE).findall(configurationText)
		selectedText+=re.compile('(end\s+\w+\s*>)',re.IGNORECASE).findall(configurationText)
		for text in selectedText:
			configurationText=re.sub(text,text.lower(),configurationText)
		verboseCommand=re.compile('(>\s*verbose)',re.IGNORECASE).findall(configurationText)
		if(verboseCommand):self.verbose.set(1)
		projectId=re.compile('> *id *= *(\S*)\W*').findall(configurationText)
		if(projectId):self.id.set(projectId[0])
		root=re.compile('> *root *= *(\S*)\W*').findall(configurationText)
		if(root):self.root.set(root[0])
		opt=re.compile('> *opt *= *(\d)').findall(configurationText)
		if(opt):self.opt.set(opt[0])
		compare=re.compile('> *compare *= *(\d)').findall(configurationText)
		if(compare):self.compare.set(compare[0])
		sa=re.compile('> *sa *= *(\w+)').findall(configurationText)
		if(sa):sa[0].lower()
		else:sa=['n']
		if(sa[0]=="y"):
			self.sa.set(1)
			self.showRugCheckbutton()
			rug=re.compile('> *rug *= *(\w+)').findall(configurationText)
			rug=rug[0].lower()
			if(rug[0]=="y"):
				self.rug.set(1)
				color=re.compile('> *color *= *(\w+)').findall(configurationText)
				color=color[0].lower()
				self.color.set(color)
				stroke=re.compile('> *stroke *= *(\w+)').findall(configurationText)
				stroke=stroke[0].lower()
				self.stroke.set(stroke)
				self.rugOpts()
			if(rug[0]=="n"):
				self.rug.set(0)
				self.rugOpts()
		elif(sa[0]=="n"):
			self.sa.set(0)
			self.rug.set(0)
			self.showRugCheckbutton()
			self.rugOpts()
		n=re.compile('> *n *= *(\S*)\W*').findall(configurationText)
		nTag=re.compile('> *n *=.*\[(.*)\]').findall(configurationText)
		if(n)and(len(nTag[0])>=1):self.n.set(n[0]+" ["+nTag[0]+"]")
		elif(n):self.n.set(n[0])
		listOfFiles=re.compile('<\s*begin\s*files\s*(.*)\s*end\s*files\s*>',re.DOTALL).findall(configurationText)
		if(listOfFiles):
			listOfFiles=re.sub("(\[.*\])","",listOfFiles[0])
			listOfFiles=re.sub("\s","",listOfFiles)
			listOfFiles=listOfFiles.split(";")
			listOfFiles=filter(None,listOfFiles)
			self.files=listOfFiles
		pools=re.compile('<\s*begin\s*pools\s*(.*)\s*end\s*pools\s*>',re.DOTALL).findall(configurationText)
		if(pools):
			pools=re.sub("\s","",pools[0])
			self.pools.set(pools)
		clades=re.compile('<\s*begin\s*clades\s*(.*)\s*end\s*clades\s*>',re.DOTALL).findall(configurationText)
		trees=re.compile('<\s*begin\s*trees\s*(.*)\s*end\s*trees\s*>',re.DOTALL).findall(configurationText)
		if(clades)and(not(trees)):
			self.clades.set(clades[0])
			self.search.set(1)
			self.targetSearches()
		if(trees)and(not(clades)):
			self.trees.set(trees[0])
			self.search.set(2)
			self.targetSearches()
		if(clades)and(trees):
			result=messagebox.askquestion("Choose constriction option","If you want to search for the specified clade(s), press 'YES'. Otherwise, press 'NO' to restrict your search to the specified tree(s).",icon='warning')
			if(result=="yes"):
				self.clades.set(clades[0])
				self.search.set(1)
				self.targetSearches()
			elif(result=="no"):
				self.trees.set(trees[0])
				self.search.set(2)
				self.targetSearches()

	def copyrigth(self):
		messagebox.showinfo("Copyrigth",COPYRIGTH)
		return

	def manual(self):
		MESSAGE="See the files README.txt and configuration.txt that accompanies YBYRÁ.\n .\nContact the author: denisjacobmachado@gmail.com"
		messagebox.showinfo("Need help?",MESSAGE)
		return

	def cite(self):
		MESSAGE="Machado,D.J. (2015) YBYRÁ facilitates comparison of large phylogenetic trees. BMC Bioinformatics, 16, 204. doi: 10.1186/s12859-015-0642-9"
		messagebox.showinfo("How to cite",MESSAGE)
		return

	def createWidgets(self):
#		Variables
		self.files=[] # Open tree files [<begin files; end files>]
		self.id=tk.StringVar() # Project name [>id = output]
		self.id.set("output") # Set default
		self.sa=tk.IntVar() # Do sensitivity analysis [>sa = no]
		self.msdCheck=tk.IntVar() # Search for wildcard taxa
		self.rug=tk.IntVar() # Print Navajo rug [>rug = no]
		self.opt=tk.IntVar() # Which kind of comparison shall be done [>opt = 1]
		self.compare=tk.IntVar() # Compare clades or splits [>compare = 0]
		self.n=tk.StringVar() # Select a tree [>n = 1]
		self.n.set("1") # Set default
		self.pools=tk.StringVar() # Define pools of trees[<begin pools; end pools>]
		self.root=tk.StringVar() # Defines the root [>root]
		self.msdist=tk.StringVar() # Path to the MSdist executable directory
		self.msdist.set("MSdist/bin") # Set default
		self.clades=tk.StringVar() # Clades [<begin clades; end clades>]
		self.trees=tk.StringVar() # Clades [<begin trees; end trees>]
		self.color=tk.StringVar() # Background color [>color = black]
		self.color.set("black") # Set default
		self.stroke=tk.StringVar() # Stroke color [>stroke= black]
		self.stroke.set("black") # Set default
		self.text=tk.IntVar() # Ratio or percentage [>text = p]
		self.search=tk.IntVar() # Show search options
		self.verbose=tk.IntVar() # Verbose or not
#		Widgets
		self.idLabel=tk.Label(self,text="Project name:").grid(row=0,column=0,sticky="w")
		self.idEntry=tk.Entry(self,textvariable=self.id,width=35).grid(row=0,column=1,sticky="w")
		self.nLabel=tk.Label(self,text="Select a tree:").grid(row=1,column=0,sticky="w")
		self.nEntry=tk.Entry(self,textvariable=self.n,width=35).grid(row=1,column=1,sticky="w")
		self.nLabel=tk.Label(self,text="Restrictions:").grid(row=2,column=0,sticky="w")
		self.search1Radiobutton=tk.Radiobutton(self,text="Compare every clade/split",variable=self.search,value=0,command=self.targetSearches).grid(row=2,column=1,sticky="w")
		self.search2Radiobutton=tk.Radiobutton(self,text="Search for specific clades",variable=self.search,value=1,command=self.targetSearches).grid(row=3,column=1,sticky="w")
		self.search2Radiobutton=tk.Radiobutton(self,text="Search for specific trees or subtrees",variable=self.search,value=2,command=self.targetSearches).grid(row=4,column=1,sticky="w")
		self.cladesLabel=tk.Label(self,text="Clades:")
		self.cladesEntry=tk.Entry(self,textvariable=self.clades,width=35)
		self.treesLabel=tk.Label(self,text="Trees:")
		self.treesEntry=tk.Entry(self,textvariable=self.trees,width=35)
		self.poolsLabel=tk.Label(self,text="Pools:").grid(row=6,column=0,sticky="w")
		self.poolsEntry=tk.Entry(self,textvariable=self.pools,width=35).grid(row=6,column=1,sticky="w")
		self.rootLabel=tk.Label(self,text="Root:").grid(row=7,column=0,sticky="w")
		self.rootEntry=tk.Entry(self,textvariable=self.root,width=35).grid(row=7,column=1,sticky="w")
		self.optLabel=tk.Label(self,text="Option:").grid(row=8,column=0,sticky="w")
		self.opt1Radiobutton=tk.Radiobutton(self,text="1. Find exact matches",variable=self.opt,value=1).grid(row=8,column=1,sticky="w")
		self.opt2Radiobutton=tk.Radiobutton(self,text="2. List clades or splits",variable=self.opt,value=2).grid(row=9,column=1,sticky="w")
		self.opt3Radiobutton=tk.Radiobutton(self,text="3. Calculate split distance between trees",variable=self.opt,value=3).grid(row=10,column=1,sticky="w")
		self.opt4Radiobutton=tk.Radiobutton(self,text="4. Calculate global split distance",variable=self.opt,value=4).grid(row=11,column=1,sticky="w")
		self.optLabel=tk.Label(self,text="Compare:").grid(row=13,column=0,sticky="w")
		self.splitsRadiobutton=tk.Radiobutton(self,text="Splits",variable=self.compare,value=0).grid(row=13,column=1,sticky="w")
		self.cladesRadiobutton=tk.Radiobutton(self,text="Clades",variable=self.compare,value=1).grid(row=14,column=1,sticky="w")
		self.saCheckbutton=tk.Checkbutton(self,text="Enable SA",variable=self.sa,onvalue=1,offvalue=0,command=self.showRugCheckbutton).grid(row=15,column=0,sticky="w")
		self.rugCheckbutton=tk.Checkbutton(self,text="Print Navajo rugs",variable=self.rug,onvalue=1,offvalue=0,command=self.rugOpts)
		self.verboseCheckbutton=tk.Checkbutton(self,text="Verbose",variable=self.verbose).grid(row=19,column=0,sticky="w")
		self.msdistLabel=tk.Label(self,text="Path to MSdist executable directory:")
		self.msdistPath=tk.Entry(self,textvariable=self.msdist,width=35)
		self.msdistCheckbutton=tk.Checkbutton(self,text="Search for possible wildcard taxa",variable=self.msdCheck,command=self.showMSdistPath).grid(row=20,column=0,sticky="w")
		self.runButton=tk.Button(self,text="Save",command=self.showVariables).grid(row=25,column=0,sticky="w")
		self.quitButton=tk.Button(self,text="Quit",command=self.quit).grid(row=25,column=1,sticky="w")
		self.runButton=tk.Button(self,text="Save/Run",command=self.run).grid(row=26,column=1,sticky="w")
		self.colorLabel=tk.Label(self,text="Background color:")
		self.colorEntry=tk.Entry(self,textvariable=self.color,width=35)
		self.strokeLabel=tk.Label(self,text="Stroke color:")
		self.strokeEntry=tk.Entry(self,textvariable=self.stroke,width=35)
		self.opt1Radiobutton=tk.Radiobutton(self,text="Ratio",variable=self.text,value=0)
		self.opt2Radiobutton=tk.Radiobutton(self,text="Percentage",variable=self.text,value=1)

#	Functions
	def targetSearches(self):
		if self.search.get()==0:
			self.cladesLabel.grid_remove()
			self.cladesEntry.grid_remove()
			self.treesLabel.grid_remove()
			self.treesEntry.grid_remove()
		elif self.search.get()==1:
			self.cladesLabel.grid(row=5,column=0,sticky="w")
			self.cladesEntry.grid(row=5,column=1,sticky="w")
			self.treesLabel.grid_remove()
			self.treesEntry.grid_remove()
		elif self.search.get()==2:
			self.cladesLabel.grid_remove()
			self.cladesEntry.grid_remove()
			self.treesLabel.grid(row=5,column=0,sticky="w")
			self.treesEntry.grid(row=5,column=1,sticky="w")

	def showRugCheckbutton(self):
		if self.sa.get()==1:self.rugCheckbutton.grid(row=15,column=1,sticky="w")
		elif self.sa.get()==0:
			self.rugCheckbutton.grid_remove()
			self.rug.set(0)

	def showMSdistPath(self):
		if self.msdCheck.get()==1:
			self.msdistLabel.grid(row=21,column=0,sticky="w")
			self.msdistPath.grid(row=21,column=1,sticky="w")
			if self.sa.get()==1:
				self.rug.set(0)
				self.sa.set(0)
				self.colorLabel.grid_remove()
				self.colorEntry.grid_remove()
				self.strokeLabel.grid_remove()
				self.strokeEntry.grid_remove()
				self.opt1Radiobutton.grid_remove()
				self.opt2Radiobutton.grid_remove()
				self.rugCheckbutton.grid_remove()
		if self.msdCheck.get()==0:
			self.msdistLabel.grid_remove()
			self.msdistPath.grid_remove()

	def rugOpts(self):
		if self.rug.get()==1:
			self.colorLabel.grid(row=16,column=0,sticky="w")
			self.colorEntry.grid(row=16,column=1,sticky="w")
			self.strokeLabel.grid(row=17,column=0,sticky="w")
			self.strokeEntry.grid(row=17,column=1,sticky="w")
			self.opt1Radiobutton.grid(row=18,column=0,sticky="w")
			self.opt2Radiobutton.grid(row=18,column=1,sticky="w")
		elif self.rug.get()==0:
			self.colorLabel.grid_remove()
			self.colorEntry.grid_remove()
			self.strokeLabel.grid_remove()
			self.strokeEntry.grid_remove()
			self.opt1Radiobutton.grid_remove()
			self.opt2Radiobutton.grid_remove()

	def showVariables(self):
		self.configurationName=self.id.get()+".conf"
		result="yes"
		if(os.path.isfile(self.configurationName)):
			title="File %s already exists!"%self.configurationName
			question="Overwrite file %s?"%self.configurationName
			result=messagebox.askquestion(title,question,icon='warning')
		if(result=="yes"):
			manifest=open(self.configurationName,"w")
			configurationText=">id=%s\n<begin files\n"%self.id.get()
			if(self.files):
				for file in self.files:configurationText+="%s;\n"%file
			else:
				self.openFiles()
				for file in self.files:configurationText+="%s;\n"%file
			configurationText+="end files>\n"
			if self.pools.get():configurationText+="<begin pools\n%s\nend pools>\n"%self.pools.get()
			if(self.search.get()==1)and(self.clades.get()):configurationText+="<begin clades\n%s\nend clades>\n"%self.clades.get()
			if(self.search.get()==2)and(self.trees.get()):configurationText+="<begin trees\n%s\nend trees>\n"%self.clades.get()
			if self.root.get():configurationText+=">root=%s\n"%self.root.get()
			configurationText+=">opt=%s\n>n=%s\n>compare=%s\n"%(self.opt.get(),self.n.get(),self.compare.get())
			if(self.sa.get()==1):
				configurationText+=">sa=yes\n"
				if self.rug.get()==1:
					configurationText+=">rug=yes\n>stroke=%s\n>color=%s\n"%(self.stroke.get(),self.color.get())
				elif self.rug.get()==0:configurationText+=">rug=no\n"
			elif self.sa.get()==0:configurationText+=">sa=no\n>rug=no\n"
			if self.text.get()==0:configurationText+=">text=r\n"
			elif self.text.get()==1:configurationText+=">text=p\n"
			if self.verbose.get()==0:configurationText+="#>verbose\n"
			elif self.verbose.get()==1:configurationText+=">verbose\n"
			if self.msdCheck.get()==1:configurationText+=">msdist=%s\n"%(self.msdist)
			elif self.msdCheck.get()==0:configurationText+="#>msdist=%s\n"%(self.msdist)
			manifest.write(configurationText)
			manifest.close()
			messagebox.showinfo("Done!","Configuration file saved as %s.\nRun:\n$ python ybyra_sa.py -f %s"%(self.configurationName,self.configurationName))
		elif(result=="no"):messagebox.showinfo("Change the output name","To save the configuration with a different mane, edit the 'Project name' text box")

	def run(self):
		self.showVariables()
		try:import subprocess
		except ImportError:
			messagebox.showinfo("ERROR!","Could not import subprocess module.\nUnable to execute.")
			exit()
		else:
			print(">EXECUTING:\n$ python ybyra_sa.py -f %s"%(self.configurationName))
			subprocess.call("python ybyra_sa.py -f %s"%(self.configurationName),shell=True)

# Execute GUI
root=tk.Tk() # Defines root
Application(root).pack(fill="both",expand=True) # The main program starts here by instantiating the Application clas
root.title('YBYRA') # Renames the Tkinter window
root.mainloop() # Starts the application's main loop, waiting for mouse and keyboard events
