#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ybyra_apo_parser.py
# Read a tab-separated values file (.tsv) describing characters. Parses character transformation output from ybyra_apo.py
# This program is part of the YBYRÁ package. Please check YBYRÁ's licence.

# Import modules and libraries
import argparse,re,sys

# Set arguments
parser=argparse.ArgumentParser()
parser.add_argument("-i","--info",help="Character information (character no., starting from 1, and description in tab-separated values (.tsv)",type=str,required=True)
parser.add_argument("-c","--char",help="The transformation characterization from ybyra_apo.py in comma-separated values (.csv)",type=str,default="output",required=False)
args=parser.parse_args()

# Define functions
def main():
	translations=parse_info(args.info)
	transformations=parse_char(args.char)
	report(translations,transformations)
	return

def parse_info(file):
	dic={}
	handle=open(file,"r")
	for l in handle.readlines():
		l=l.strip()
		if(l):
			n,description=l.split("\t")
			dic[n]=description
	handle.close()
	return dic

def parse_char(file):
	dic={}
	handle=open(file,"r")
	for l in handle.readlines():
		l=l.strip()
		if(l):
			if("''" in l or "'NODE'" in l or "'Nodes'" in l):
				flag=re.sub("\s+\(.+\)","",re.compile("'.*?'\s*,\s*'(.+)'").findall(l)[0])
				flag=flag[0].upper()+flag[1:].lower()
			else:
				cols=re.compile("'(.+?)'").findall(l)
				node=cols[0]
				for c in cols[1:]:
					n,t=re.compile("(\d+)[,\(]([^,\(\)]+)").findall(c)[0]
					key="{}|{}".format(flag,n)
					if(key in dic):
						dic[key]+=["{}|{}".format(t,node)]
					else:
						dic[key]=["{}|{}".format(t,node)]
	handle.close()
	return dic

def report(translations,transformations):
	type=""
	for key in sorted(transformations):
		flag,character=key.split("|")
		if(flag==type):
			pass
		else:
			type=flag
			sys.stdout.write("{}:\n".format(type))
		sys.stdout.write("\tCharacter No. {} = {}\n".format(character,translations[character]))
		for subkey in transformations[key]:
			states,node=subkey.split("|")
			states=states.replace("-","->")
			sys.stdout.write("\t\tCharacter-state {} in {}\n".format(states,node))
	return

# Execute functions
main()

# Quit
exit()
