#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ybyra_apo_polish.py
# Takes output from ybyra_apo.py to create .svg plots of non-ambiguous synapomorphies including the plesiomorphic state.
# This program is part of the YBYRÁ package. Please check YBYRÁ's licence.

# Import modules and libraries
import argparse,sys,re

# Set arguments
parser=argparse.ArgumentParser()
parser.add_argument("-c1","--csv_simple",help="YBYRA's csv output with all non-ambiguous synapomorphies",type=str,required=True)
parser.add_argument("-c2","--csv_caracterization",help="YBYRA's csv output with complete caracterization of transformations",type=str,required=True)
parser.add_argument("-r1","--min_range",help="Print from this character (min-max; leave it blank to print all)",type=int,required=False)
parser.add_argument("-r2","--max_range",help="Print up to this character (min-max; leave it blank to print all)",type=int,required=False)
parser.add_argument("-s","--svg_constraint",help="print only selected type of transformations (1. unique, non-homoplastic; 2. unique, homoplastic; 3. non-unique, homoplastic; 4. ambiguous; leave it blank to print them all)",type=int,nargs="+",choices=[1,2,3,4],required=False)
parser.add_argument("--font_size",help="font-size in px for SVG plots (default = 10)",type=int,default=10,required=False)
parser.add_argument("--font_weight",help="font-weight (e.g., normal, bold, bolder, lighter) for SVG plots (default = bold)",type=str,default="bold",required=False)
parser.add_argument("--text_position",help="vertical text adjustment for characters (default = 0)",type=int,default=0,required=False)
parser.add_argument("-t", "--text", help="Print polished results to the standard output (comma-separated values; .cvs) instead of printing SVG plots", action="store_true")
args=parser.parse_args()

# Define functions
def main():
	ples=parse_csv1(args.csv_simple)
	blacks,reds,blues,whites=parse_csv2(ples,args.csv_caracterization)
	if args.text:
		report(ples,blacks,reds,blues,whites)
	else:
		print_svg(ples,blacks,reds,blues,whites)
	return

def parse_csv1(file): # parses the csv file will all uncategorized non-ambiguous synapomorphies
	ples={}
	handle=open(file,"r")
	for line in handle.readlines():
		line=line.strip()
		if(not line):
			pass
		else:
			if("'NODE'" in line)or("'See" in line):
				pass
			else:
				cols=re.compile("'(.+?)'").findall(line)
				node=cols[0]
				ples[node]=[]
				for trans in cols[1:]:
					char,states=re.compile("(.+)\((.+)\)").findall(trans)[0]
					ples[node]+=[[char,states]]
	handle.close()
	return ples

def parse_csv2(ples,file): # parses the csv file will all categorized transformations
	blacks={} # Black cells are unique, non-homoplastic synapomorphies
	reds={} # Red cells are unique, homoplastic synapomorphies
	blues={} # Blue cells are non-unique, homoplastic synapomorphies
	whites={} # White cells are ambiguous optimized synapomorphies
	if(args.svg_constraint):
		svg_constraint=args.svg_constraint
	else:
		svg_constraint=[1,2,3,4]
	handle=open(file,"r")
	for line in handle.readlines():
		line=line.strip()
		if(not line):
			pass
		else:
			if("'Nodes'," in line)or("''," in line):
				flag=re.compile("'.*?'\s*,\s*'(.+) \(.+'").findall(line)[0].strip()
			elif(flag!="Unambiguous synapomorphies"):
				cols=re.compile("'(.+?)'").findall(line)
				node=cols[0]
				if(not node in ples):
					pass
				else:
					for trans in cols[1:]:
						try:
							char=re.compile("(.+?),.+").findall(trans)[0]
						except:
							print(line)
							print(trans)
							exit()
						include=True
						if(args.min_range):
							if(int(char)<args.min_range):
								include=False
						if(args.max_range):
							if(int(char)>args.max_range):
								include=False
						if(include==True):
							if(flag=="Exclusive synapomorphies")and(1 in svg_constraint):
								if(node in blacks):
									blacks[node]+=[char]
								else:
									blacks[node]=[char]
							if(flag=="Non-private synapomorphies")and(2 in svg_constraint):
								if(node in reds):
									reds[node]+=[char]
								else:
									reds[node]=[char]
							if(flag=="Private synapomorphies")and(3 in svg_constraint):
								if(node in blues):
									blues[node]+=[char]
								else:
									blues[node]=[char]
							if(flag=="Exclusive character states")and(4 in svg_constraint):
								if(node in whites):
									whites[node]+=[char]
								else:
									whites[node]=[char]
	return blacks,reds,blues,whites

def print_svg(ples,blacks,reds,blues,whites): # This function prints the SVG plots
	try:
		import svgwrite
	except:
		print(">ERROR: Could not find package svgwrite.\n[Package information: <https://pypi.python.org/pypi/svgwrite/>]")
		exit()
	for node in sorted(list(set([node for dic in [blacks,reds,blues,whites] for node in dic]))):
		svgFilename="%s.polished.svg"%(re.sub("\s","_",node))
		dwg=svgwrite.Drawing(filename=svgFilename,size=(u'100%', u'100%'))
		states={}
		characters={}
		for x in ples[node]: # the order of the operations inside this loop is important
			states[int(x[0])]=x[1]
			if(node in whites):
				if(x[0] in whites[node]):
					characters[int(x[0])]="white"
			if(node in blues):
				if(x[0] in blues[node]):
					characters[int(x[0])]="blue"
			if(node in reds):
				if(x[0] in reds[node]):
					characters[int(x[0])]="red"
			if(node in blacks):
				if(x[0] in blacks[node]):
					characters[int(x[0])]="black"
		x=0
		for CHAR in sorted(characters):
			x+=1
			STATE=states[CHAR]
			COLOR=characters[CHAR]
			CHAR=str(CHAR)
			if(COLOR=="white"):
				TEXT="black"
			else:
				TEXT="white"
			X1=(x-1)*25
			X2=12.5+(x-1)*25
			dwg.add(dwg.rect(insert=(X1,0),
				size=("25px","25px"),
				stroke_width="1",
				stroke="black",
				fill=COLOR))
			dwg.add(dwg.text(STATE,
				insert=(X2,17.5),
				fill=TEXT,
				style="font-size:%dpx;font-family:Arial;font-weight:%s;text-anchor:middle"%(args.font_size,args.font_weight)))
			dwg.add(dwg.text(CHAR,
				insert=(X2,35+args.text_position),
				fill="black",
				style="font-size:%dpx;font-family:Arial;font-weight:%s;text-anchor:middle"%(args.font_size,args.font_weight)))
		dwg.save()
	return

def report(ples,blacks,reds,blues,whites): # This function prints the SVG plots
	sys.stdout.write("Node,Character,Plesiomorphic,Apomorphic,Unique,Homoplastic\n")
	for node in sorted(list(set([node for dic in [blacks,reds,blues,whites] for node in dic]))):
		states={}
		characters={}
		for x in ples[node]: # the order of the operations inside this loop is important
			states[int(x[0])]=x[1]
			if(node in whites):
				if(x[0] in whites[node]):
					characters[int(x[0])]="NA,NA"
			if(node in blues):
				if(x[0] in blues[node]):
					characters[int(x[0])]="No,Yes"
			if(node in reds):
				if(x[0] in reds[node]):
					characters[int(x[0])]="Yes,Yes"
			if(node in blacks):
				if(x[0] in blacks[node]):
					characters[int(x[0])]="Yes,No"
		for CHAR in sorted(characters):
			STATE=states[CHAR]
			COLOR=characters[CHAR]
			CHAR=str(CHAR)
			i, j = re.compile("(.+)-(.+)").findall(STATE)[0]
			m, n = COLOR.split(",")
			sys.stdout.write("{},{},{},{},{},{}\n".format(node,CHAR, i, j, m, n))
	return

# Execute functions
main()

# Quit
exit()
