# A simple example of character categorization

## Execute the categorization analysis

First, execute character chategorization with `ybyra_apo.py` (you may need to provite the full path to the `tnt` program in your computer):

`python3 ../ybyra_apo.py -t example.tre -m example.nex -S -T /path/to/tnt`

Executed this way, the files `OUTPUT.csv` and `OUTPUT_characters.csv` will be created. You may use this files to "polish" the results.

## Create SVG plots with plesiomorphic and autapomorphic character states

You may create "polished" SVG plots with plesiomorphic states using `ybyra_apo_polish.py`:

`python3 ../ybyra_apo_polish.py -c1 OUTPUT.csv -c2 OUTPUT_characters.csv`

Note that ambiguous transformations will be ignored by `ybyra_apo_polish.py`.

## Expected results

See the `OUTPUT.csv` and `OUTPUT_characters.csv` file that comes with this example. For a visual summary of the expected results, see https://gitlab.com/MachadoDJ/ybyra/-/wikis/Output#svg-files.
