# Combining Newick Trees

## Problem and solution

Imagine you need to merge the information from all or some of different tree files in Newick notation (sometimes called PHYLIP format) described below:

1. consensus tree
2. one binary tree with branch lengths
3. support tree with Goodman-Bremer values
4. a tree with clade frequencies (bootstrap values)
5. a tree with node numbers from the analysis made with YBYRÁ

You can merge these Newick trees with `merging_newick_trees.py`. Using the example files that come with the script, you may execute the following command line:

```
python3 merging_newick_trees.py -n nodes.nwk -c consensus.nwk -l length.nwk -l1 "support" -s1 support.nwk -l2 "bootstrap" -s2 bootstrap.nwk
```

### Additional usage information

Run `python merging_newick_trees.py --help` for more usage details.

```
usage: merging_newick_trees.py [-h] [-n NODES] -c CONSENSUS [-l LENGTH] [-s1 SUPPORT1] [-l1 LABEL1] [-s2 SUPPORT2]
                               [-l2 LABEL2] [-o OUTPUT]

optional arguments:
  -h, --help            show this help message and exit
  -n NODES, --nodes NODES
                        Tree with node numbers
  -c CONSENSUS, --consensus CONSENSUS
                        Consensus tree
  -l LENGTH, --length LENGTH
                        Tree with branch lengths
  -s1 SUPPORT1, --support1 SUPPORT1
                        First tree with primary support of frequency values (T1)
  -l1 LABEL1, --label1 LABEL1
                        Label for the values in S1 (L1) [deafult=support]
  -s2 SUPPORT2, --support2 SUPPORT2
                        Second tree with different support of frequency values (S2)
  -l2 LABEL2, --label2 LABEL2
                        Label for the values in S2 (L2) [default=bootstrap]
  -o OUTPUT, --output OUTPUT
                        Prefix for the names of output files [default=output]
```

## Output description

Error messages and warnings will be printed into the standard output.

The main  results (viz., a NEXUS file with the tree and node information) will be printed into the standard output.

Two files will be created: a tree file in NEXUS format with node values (inclusing support values and branch lengths) and a table in tab separated values containing the branch length and support values from each node.

# Summarizing information for complex molecular data

Please see the `ybyra_table.py` script in this subdirectory. This script was created to help summarize molecular data on categorized transformation per node in a complex tree. This tree is expected to have two support values, node labels, and branch lengths. Also, the matrix is expected to be molecular with multiple partitions.

Run `python ybyra_table.py --help` for more usage details.

```
usage: ybyra_table.py [-h] [-c CHARACTERS] -a ALIGNMENT -p PARTITIONS -t TREE [-o OUTPUT] [-k LABEL1] [-l LABEL2]
                      [-m LABEL3]

options:
  -h, --help            show this help message and exit
  -c CHARACTERS, --characters CHARACTERS
                        CSV table from YBYRÁ with the categorized characters
  -a ALIGNMENT, --alignment ALIGNMENT
                        Alignment in simplified NEXUS format
  -p PARTITIONS, --partitions PARTITIONS
                        Partitions file in NEXUS format
  -t TREE, --tree TREE  Tree in Nexus format (expected to contain: branch lengths, node labels, and two support
                        metrics; NEXUS file produced with `merging_newick_trees.py`)
  -o OUTPUT, --output OUTPUT
                        Path to the CSV output table
  -k LABEL1, --label1 LABEL1
                        Label for node values
  -l LABEL2, --label2 LABEL2
                        Label for first support value
  -m LABEL3, --label3 LABEL3
                        Label for second support value
```