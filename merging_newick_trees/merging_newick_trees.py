#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# combine_nwk_trees.py
# Homemade Python script to merge information from Newick trees containing the consensus,
# branch lengths, node numbers, and support values

# Log messages are printed to stderr
# Final results are printed to stdout

# Import modules and libraries
import argparse,sys,re

# Set arguments
parser=argparse.ArgumentParser()
parser.add_argument("-n","--nodes",help="Tree with node numbers",type=str,required=False)
parser.add_argument("-c","--consensus",help="Consensus tree",type=str,required=True)
parser.add_argument("-l","--length",help="Tree with branch lengths",type=str,required=False)
parser.add_argument("-s1","--support1",help="First tree with primary support of frequency values (T1)",type=str,required=False)
parser.add_argument("-l1","--label1",help="Label for the values in S1 (L1) [deafult=support]",type=str,required=False,default="support",)
parser.add_argument("-s2","--support2",help="Second tree with different support of frequency values (S2)",type=str,required=False)
parser.add_argument("-l2","--label2",help="Label for the values in S2 (L2) [default=bootstrap]",type=str,required=False,default="bootstrap")
parser.add_argument("-o","--output",help="Prefix for the names of output files [default=output]",type=str,required=False,default="output")
args=parser.parse_args()


# Test to see if at least one tree file is available:
test = False
for path in [args.support1, args.support2, args.length, args.nodes]:
	if path:
		test = True
		break
if not test:
	sys.stderr.write("! Error: not enough trees to merge\n")
	exit()

# Define functions tc,tb,tl,tn,ts
def check_terminals(tc,tn,bl,s1,s2):
	otus=[]
	for file in [tc,tn,bl,s1,s2]:
		if file:
			handle=open(file,"r")
			t=handle.read()
			handle.close()
			t=re.sub("\s","",t)
			t=re.sub(":[^,;\(\)]+","",t)
			l=set(re.compile("[^,;\(\)]+").findall(t))
			if(otus):
				if(not l==otus):
					sys.stderr.write("! ERROR: Terminals do not match\n")
					sys.stderr.write("{}\n".format(set(l)-set(otus)))
					exit()
				else:
					sys.stderr.write("Terminals match in {}\n".format(file))
					pass
			else:
				otus=l
	return sorted(list(otus))

def get_consensus_clades(tc):
	clades={}
	cnodes={}
	handle=open(tc,"r")
	t=handle.read()
	handle.close()
	t=re.sub("\s","",t)
	t=re.sub("\[.+?\]","",t)
	t=re.sub(":[^,;\(\)]+","",t)
	ncount=0
	while "(" in t:
		innerclades=re.compile("(\([^\(\)]+\))").findall(t)
		for item in innerclades:
			name=",".join(sorted(re.compile("[^,;\(\)\[\]]+").findall(item))) # A clade's name will be all the names of it's sorted terminals
			clades[name]=[]
			ncount+=1
			cnodes[name]=str(ncount)
			rplc=item.replace("(","[")
			rplc=rplc.replace(")","]")
			t=t.replace(item,rplc)
	return clades, cnodes

def add_length(otus_list,clades,bl):
	otus={}
	if bl:
		handle=open(bl,"r")
		t=handle.read()
		handle.close()
		t=re.sub("\s","",t)
		for otu in re.compile("([^,;\(\)]+:[^,;\(\)]+)").findall(t):
			name,value=otu.split(":")
			otus[name]=value
			t=t.replace(otu,name)
		while "(" in t:
			innerclades=re.compile("(\([^\(\)]+\)[^,;\(\)\[\]]*)").findall(t)
			for item in innerclades:
				try:
					name,value=item.split(":")
				except:
					pass
				else:
					name=",".join(sorted(re.compile("[^,;\(\)\[\]]+").findall(name))) # A clade's name will be all the names of it's sorted terminals
					if(name in clades):
						clades[name]+=[value]
				rplc=item.replace("(","[")
				rplc=rplc.replace(")","]")
				rplc=re.sub(":[^,;\(\)\[\]]+","",rplc)
				t=t.replace(item,rplc)
	else:
		sys.stderr.write("! Warning: in the absence of a tree with branch lengths, branch lengths in the consensus tree were all set to 1.\n")
		for otu in otus_list:
			otus[otu] = 1
		for clade in clades:
			clades[clade] += [1]
	return otus,clades

def add_nodes(clades,tn,cnodes):
	if tn:
		handle=open(tn,"r")
		t=handle.read()
		handle.close()
		t=re.sub("\s","",t)
		for otu in re.compile("([^,;\(\)]+:[^,;\(\)]+)").findall(t):
			name,value=otu.split(":")
			t=t.replace(otu,name)
		while "(" in t:
			innerclades=re.compile("(\([^\(\)]+\):[^,;\(\)\[\]]+)").findall(t)
			for item in innerclades:
				name,value=item.split(":")
				name=",".join(sorted(re.compile("[^,;\(\)\[\]]+").findall(name))) # A clade's name will be all the names of it's sorted terminals
				if(name in clades):
					clades[name]+=[value]
				rplc=item.replace("(","[")
				rplc=rplc.replace(")","]")
				rplc=re.sub(":[^,;\(\)\[\]]+","",rplc)
				t=t.replace(item,rplc)
		for key in clades:
			if(len(clades[key])==1):
				clades[key]+=["na"]
	else:
		sys.stderr.write("! Warning: in the absence of a tree with node numbers, node were numbered automatically from the consensus tree.\n")
		for name in clades:
			clades[name]+=[cnodes[name]]
	return clades

def add_support1(clades,ts):
	if ts:
		handle=open(ts,"r")
		t=handle.read()
		handle.close()
		t=re.sub("\s","",t)
		while "):" in t:
			innerclades=re.compile("(\([^\(\)]+\):[^,;\(\)\[\]]+)").findall(t)
			for item in innerclades:
				name,value=item.split(":")
				name=",".join(sorted(re.compile("[^,;\(\)\[\]]+").findall(name))) # A clade's name will be all the names of it's sorted terminals
				if(name in clades):
					clades[name]+=[value]
				rplc=item.replace("(","[")
				rplc=rplc.replace(")","]")
				rplc=re.sub(":[^,;\(\)\[\]]+","",rplc)
				t=t.replace(item,rplc)
		for key in clades:
			if(len(clades[key])==2):
				clades[key]+=["na"]
	else:
		sys.stderr.write("! Warning: in the absence of a tree with support or frequency values (S1), values in the consensus tree were all set to 'na'\n")
		for key in clades:
			clades[key]+=["na"]
	return clades

def add_support2(clades,tb):
	if tb:
		handle=open(tb,"r")
		t=handle.read()
		handle.close()
		t=re.sub("\s","",t)
		while "):" in t:
			innerclades=re.compile("(\([^\(\)]+\):[^,;\(\)\[\]]+)").findall(t)
			for item in innerclades:
				name,value=item.split(":")
				name=",".join(sorted(re.compile("[^,;\(\)\[\]]+").findall(name))) # A clade's name will be all the names of it's sorted terminals
				if(name in clades):
					clades[name]+=[value]
				rplc=item.replace("(","[")
				rplc=rplc.replace(")","]")
				rplc=re.sub(":[^,;\(\)\[\]]+","",rplc)
				t=t.replace(item,rplc)
		for key in clades:
			if(len(clades[key])==3):
				clades[key]+=["na"]
	else:
		sys.stderr.write("! Warning: in the absence of a tree with with support or frequency values (S2), values in the consensus tree were all set to 'na'\n")
		for key in clades:
			clades[key]+=["na"]
	return clades

def report(otus,clades,tc):
	handle=open(tc,"r")
	t=handle.read()
	handle.close()
	# Here I will temporarily add quotes to terminal names to facilitate replacement
	t=re.sub("\s","",t)
	t=re.sub(":[^,;\(\)]+","",t)
	t=re.sub("([\(,;\)])([^\(,;\)])",r"\1'\2",t)
	t=re.sub("([^\(,;\)])([\(,;\)])",r"\1'\2",t)
	data="Node number\tBranch length\t{}\t{}\n".format(args.label1,args.label2)
	for key in otus:
		t=t.replace("'{}'".format(key),"{}':{}'".format(key,otus[key])) # Here I add quotes to node info to facilitate parsing
	while "(" in t:
		innerclades=re.compile("(\([^\(\)]+\))").findall(t)
		for item in innerclades:
			fnd=item
			item=re.sub("'[^']+'","",item)
			name=",".join(sorted(re.compile("[^,;\(\)\[\]\{\}]+").findall(item)))
			try:
				length=clades[name][0]
			except:
				length="na"
			try:
				node=clades[name][1]
			except:
				node="na"
			try:
				support1=clades[name][2]
			except:
				support1="na"
			try:
				support2=clades[name][3]
			except:
				support2="na"
			rplc="{}'[&{}={},node={},{}={}]:{}'".format(fnd,args.label1,support1,node,args.label2,support2,length) # Here I add quotes to node info to facilitate parsing
			# Nodes that already have been visited are put between curly brackets to facilitate parsing
			rplc=rplc.replace("(","{")
			rplc=rplc.replace(")","}")
			t=t.replace(fnd,rplc)
			data+="{}\t{}\t{}\t{}\n".format(node,length,support1,support2)
	t=re.sub("\'","",t)
	t=re.sub("\{","(",t)
	t=re.sub("\}",")",t)
	t=t.replace("[&support=na,node=na,bootstrap=na]","")
	t=t.replace(",node=na","")
	t=t.replace(",bootstrap=na","")
	sys.stdout.write("#NEXUS\n\nbegin trees;\n\ttree CONSENSUS = [&R] {}\nend;\n".format(t))
	handle = open("{}_merged.nexus".format(args.output), "w")
	handle.write(t)
	handle.close()
	handle = open("{}_data.tsv".format(args.output), "w")
	handle.write(data)
	handle.close()
	return

def main():
	# Main variables:
	s1=args.support1
	s2=args.support2
	tc=args.consensus
	bl=args.length
	tn=args.nodes
	# Check if terminals match
	otus=check_terminals(tc,tn,bl,s1,s2) # This is only to check that terminals are the same in all trees
	sys.stderr.write("Found {} terminals\n".format(len(otus)))
	# Get clades from the consensus tree:
	clades,cnodes=get_consensus_clades(tc)
	sys.stderr.write("Found {} clades in the consensus\n".format(len(clades)))
	sys.stderr.write("Aquiring node information from different trees\n")
	# Add branch lengths:
	otus,clades=add_length(otus,clades,bl)
	# Get node numbers:
	clades=add_nodes(clades,tn,cnodes)
	# Extract support values from up up to two different trees:
	clades=add_support1(clades,s1)
	clades=add_support2(clades,s2)
	# Report:
	sys.stderr.write("Editing consensus tree\n")
	report(otus,clades,tc)
	sys.stderr.write("Done\n")
	return

# Execute functions
if __name__ == "__main__":
	main()

# Quit
exit()
