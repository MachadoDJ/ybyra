#!/usr/bin/env python3

##
# MODULES
##

import argparse, re, sys

##
# SECTION I: Reading the input data
##

# Read the character file --> get the type and the transformations in each node (dictionary)
def read_characters_file(characters_file):
	transformations_dic = {}
	handle = open(characters_file, "r")
	for line in handle.readlines():
		line = line.strip()
		if line:
			if "(character no.: character state)" in line:
				kind = re.compile(",\'(.+?)\s*\(").findall(line)[0]
			else:
				cells = [i.strip() for i in re.compile("'(.+?)'").findall(line)]
				node = cells[0]
				try:
					node = int(re.compile("Node\s+(\d+)").findall(node.strip())[0])
				except:
					node = str(node)
				transformations_dic[node] = []
				for pos, char in [i.split(",") for i in cells[1:]]:
					transformations_dic[node].append([int(pos), char, kind])
	handle.close()
	return transformations_dic

# Read the alignment file --> get the sequences (dictionary)
def read_alignment_file(alignment_file):
	align_dic = {}
	handle = open(alignment_file, "r")
	align_data = handle.read()
	handle.close()
	matrix = re.compile(";\s*MATRIX\s*(.+?);",re.MULTILINE|re.IGNORECASE|re.DOTALL).findall(align_data)[0].strip().split("\n")
	for line in matrix:
		line = line.strip()
		if line:
			terminal, sequence = [i.strip() for i in re.compile("^(.+)\s+([^\s]+)$").findall(line)[0]]
			align_dic = sequence
	return align_dic

# Read the partitions file --> get the start and end positions of each gene in the alignment (dictionary)
def read_partitions(partitions_file):
	partitions_dic = {}
	handle = open(partitions_file, "r")
	partitions_data = handle.read()
	handle.close()
	sets = re.compile("BEGIN\s+SETS\s*;(.+)END;",re.MULTILINE|re.DOTALL|re.IGNORECASE).findall(partitions_data)[0].strip()
	for pname, pstart, pend in re.compile("([^\s]+)\s*=\s*(\d+)\s*-\s*(\d+)").findall(sets):
		partitions_dic[pname] = [int(pstart)-1, int(pend)-1]
	return partitions_dic

# Read the tree file --> get terminals, clades, node numbers, and node values (terminals as a dictionary, clades as a dictionary of lists, and a dictionary for each node values including branch length, support, and jackknife)
def read_tree(tree_file, node_label, support1_label, support2_label):
	terminals_dic = {}
	clades_dic = {}
	gb_dic = {}
	jk_dic = {}
	bl_dic = {}
	handle = open(tree_file, "r")
	tree_data = handle.read()
	handle.close()
	tree = re.compile("tree\s[^\(]+(.+?);",re.MULTILINE|re.DOTALL).findall(tree_data)[0]
	# Process the terminals
	terminals_with_bl = re.compile("[^\(\),;\[\]]+:\d+",re.MULTILINE|re.DOTALL).findall(tree_data)
	for terminal_with_bl in terminals_with_bl:
		terminal, bl = terminal_with_bl.strip().split(':')
		terminals_dic[terminal] = bl
		tree = tree.replace(terminal_with_bl, terminal)
	# Process internal nodes
	while "(" in tree:
		internal_clades = re.compile("\([^\(\)]+\)[^\(\)]*",re.MULTILINE|re.DOTALL).findall(tree)
		for internal_clade in internal_clades:
			terminals = [terminal.strip() for terminal in re.compile("\((.+?)\)",re.MULTILINE|re.DOTALL).findall(internal_clade)[0].split(",")]
			try:
				gb = int(re.compile(f"{support1_label}=(\d+)",re.MULTILINE|re.DOTALL).findall(internal_clade)[0])
				node = int(re.compile(f"{node_label}=(\d+)",re.MULTILINE|re.DOTALL).findall(internal_clade)[0])
				jk = int(re.compile(f"{support2_label}=(\d+)",re.MULTILINE|re.DOTALL).findall(internal_clade)[0])
				bl = int(re.compile(":(\d+)",re.MULTILINE|re.DOTALL).findall(internal_clade)[0])
				gb_dic[node] = gb
				jk_dic[node] = jk
				bl_dic[node] = bl
				clades_dic[node] = terminals
			except:
				pass
			fnd = internal_clade
			rpl = ",".join(terminals)
			if internal_clade.strip()[-1] == ",":
				rpl += ","
			tree = tree.replace(fnd, rpl)
	return terminals_dic, clades_dic, gb_dic, jk_dic, bl_dic

##
# SECTION II: Create a table putting together transformations, their type (category), and the branch values
##

def report_transformations_per_node(transformations_dic, partitions_dic, gb_dic, jk_dic, bl_dic, node_label, support1_label, support2_label, output_table):
	my_table = ""
	new_line = f"{node_label},Partition,Position,State,{support1_label},{support2_label},Length,Type\n"
	sys.stdout.write(new_line)
	my_table += new_line
	for node in transformations_dic:
		node = 892
		values = []
		for dic in [gb_dic, jk_dic, bl_dic]:
			if node in dic:
				values.append(dic[node])
			else:
				values.append("?")
		gb, jk, bl = values
		map = {}
		for pname in partitions_dic:
			pstart, pend = partitions_dic[pname]
			for i in range(pstart, pend + 1):
				map[i] = pname
		for transformation in transformations_dic[node]:
			pos, char, kind = transformation
			pname = map[pos]
			new_line = f"{node},{pname},{pos},{char},{gb},{jk},{bl},{kind}\n"
			sys.stdout.write(new_line)
			my_table += new_line
	handle = open(output_table, "w")
	handle.write(my_table)
	handle.close()
	return

##
# MAIN
##

def main(characters_file, alignment_file, partitions_file, tree_file, output_table, node_label, support1_label, support2_label):
	terminals_dic, clades_dic, gb_dic, jk_dic, bl_dic = read_tree(tree_file, node_label, support1_label, support2_label)
	partitions_dic = read_partitions(partitions_file)
	transformations_dic = read_characters_file(characters_file)
	align_dic = read_alignment_file(alignment_file)
	report_transformations_per_node(transformations_dic, partitions_dic, gb_dic, jk_dic, bl_dic, node_label, support1_label, support2_label, output_table)
	return

##
# ARGUMENTS
##

if __name__ == "__main__":
	parser = argparse.ArgumentParser(description="This script was created to help summarize molecular data on categorized transformation per node in a complex tree. This tree is expected to have two support values, node labels, and branch lengths. Also, the matrix is expected to be molecular with multiple partitions.")
	# Add arguments
	parser.add_argument("-c", "--characters", type=str, required=False, help="CSV table from YBYRÁ with the categorized characters", default="OUTPUT_characters.csv")
	parser.add_argument("-a", "--alignment", type=str, required=True, help="Alignment in simplified NEXUS format")
	parser.add_argument("-p", "--partitions", type=str, required=True, help="Partitions file in NEXUS format")
	parser.add_argument("-t", "--tree", type=str, required=True, help="Tree in Nexus format (expected to contain: branch lengths, node labels, and two support metrics; NEXUS file produced with `merging_newick_trees.py`)")
	parser.add_argument("-o", "--output", type=str, required=False, help="Path to the CSV output table", default="output.csv")
	parser.add_argument("-k", "--label1", type=str, required=False, help="Label for node values", default="node")
	parser.add_argument("-l", "--label2", type=str, required=False, help="Label for first support value", default="GB")
	parser.add_argument("-m", "--label3", type=str, required=False, help="Label for second support value", default="Jacknife")
	# Parse the command-line arguments
	args = parser.parse_args()
	main(args.characters, args.alignment, args.partitions, args.tree, args.output, args.label1, args.label2, args.label3)

# Exit
exit()