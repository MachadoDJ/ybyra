#!/usr/bin/env python3
#-*- coding: utf-8 -*-

# ybyra_wild.py

# YBYRÁ - Copyright (C) 2014 - Denis Jacob Machado
# GNU General Public License version 3.0

# Denis Jacob Machado
# denisjacobmachado@gmail.com
# www.ib.usp.br/grant/anfibios/researchSoftware.html

# REQUEREMENTS
# Python version 3.*
# One or more files with trees written in Newick notation;
# Trees must be rooted in the same terminal;
# Rooting is possible and requires Bio.Phyl;
# Tress must be the same size and terminal names should be the same in all trees


###MODULES AND LIBRARIES###
import argparse,re,sys,os.path

###GET ARGUMENTS###
parser=argparse.ArgumentParser()
parser.add_argument("-c","--constraint",help="constraint list of terminals to analyze (all terminals will be analyzed by default)",type=str)
parser.add_argument("-o","--output",help="prefix for the output files (only when rooting trees)",type=str,default="OUTPUT")
parser.add_argument("-r","--root",help="re-root trees at this terminal (requires Bio.Phylo)",type=str)
parser.add_argument("-t","--trees",help='Tree files in Newick format',metavar="<trees>",type=str,nargs='+',required=True)
parser.add_argument("-v","--verbose",help="increase output verbosity",action="store_true")
parser.add_argument("-V","--version",help="print version and quit",action="store_true")
args=parser.parse_args()

if(args.version):
	sys.stdout.write("> ybyra_wild.py Version 2016.03.07\n")
	exit()

if(args.verbose):
	COPYRIGTH="""
ybyra_wild.py is part of the YBYRÁ project

YBYRÁ - Copyright (C) 2014 - Denis Jacob Machado

	YBYRÁ comes with ABSOLUTELY NO WARRANTY!
	These are free software, and you are welcome to
	redistribute them under certain conditions
	For additional information, please visit:
	http://opensource.org/licenses/GPL-3.0

	YBYRÁ is available at
	www.ib.usp.br/grant/anfibios/researchSoftware.html
"""
	sys.stdout.write("%s\n"%(COPYRIGTH))

def checkFiles():
	if(args.verbose):
		sys.stderr.write("> Selected files:\n")
	for tree in args.trees:
		if(args.verbose):
			sys.stderr.write("-- %s\n"%(tree))
		if(not os.path.isfile(tree)):
			sys.stderr.write("! ERROR: can't find file %s\n"%(tree))
			exit()
	return

def readTrees():
	all_trees_str=""
	for tree in args.trees:
		handle=open(tree,"r")
		all_trees_str+=handle.read()
		handle.close()
	if(args.verbose):
		sys.stderr.write("> %d trees total\n"%(all_trees_str.count(";")))
	return all_trees_str

def prepTrees(all_trees_str):
	all_trees_str_clean=re.sub("\s","",all_trees_str)
	all_trees_str=""
	all_trees_str_clean=re.sub("\)[^\(\)\[\],;]+",")",all_trees_str_clean)
	all_trees_str_clean=re.sub(":[^\(\)\[\],;]+","",all_trees_str_clean)
	all_trees_str_clean=re.sub("\[[^\]]+\]","",all_trees_str_clean)
	all_trees_list=all_trees_str_clean.split(";")
	all_trees_str_clean=""
	all_trees_list=filter(None, all_trees_list)
	return list(all_trees_list)

def reRoot(all_trees_list):
	if(args.verbose):
		sys.stdout.write("> Re-rooting trees at %s\n"%(args.root))
	from io import StringIO
	try:
		from Bio import Phylo
	except:
		sys.stdout.write("! ERROR: check if Bio.Phylo is available\n")
	rooted_trees=[]
	for i in range(0,len(all_trees_list)):
		if(args.verbose):
			sys.stdout.write("-- re-rooting tree # %d of %d\n"%(i+1,len(all_trees_list)))
		tree=all_trees_list[i]
		tree=Phylo.read(StringIO(tree),"newick")
		tree.root_with_outgroup(args.root)
		rooted_trees+=[tree]
	if(args.verbose):
		sys.stdout.write("-- re-rooting completed\n")
	Phylo.write(rooted_trees,"%s.rooted.trees"%(args.output),"newick")
	rooted_trees=[]
	tree=""
	all_trees_list=[]
	handle=open("%s.rooted.trees"%(args.output),"r")
	all_trees_str=handle.read()
	handle.close()
	all_trees_list=prepTrees(all_trees_str)
	return all_trees_list

def parseTrees(all_trees_list):
	clades={}
	dic={}
	if(args.verbose):
		sys.stdout.write("> Listing clades on every tree\n")
	for i in range(0,len(all_trees_list)):
		if(args.verbose):
			sys.stdout.write("-- tree # %d of %d\n"%(i+1,len(all_trees_list)))

		tree=all_trees_list[i]
		if(i==0):
			terminals=sorted(re.compile("([^\(\),;]+)").findall(tree))
			for j in range(0,len(terminals)):
				dic[terminals[j]]=j
		clades[i]=getClades(tree,dic)
	return clades,dic

def getClades(tree,dic):
	all_clades=[]
	while "(" in tree:
		clades=re.compile("\(([^\(\)]+)\)").findall(tree)
		for clade in clades:
			tree=tree.replace("(%s)"%(clade),"[%s]"%(clade))
			clade=sorted(re.compile("([^,;]+)").findall(re.sub("[\[\]\s;]","",clade)))
			all_clades+=[[dic[x] for x in clade]]
	return all_clades

def rank(clades,dic):
	dic={v: k for k, v in dic.items()}
	rank={}
	if(args.verbose):
		sys.stdout.write("> Calculating the number of combined clades in trimmed topologies\n")
	for i in range(0,len(dic)):
		if(args.constraint):
			if(not os.path.isfile(args.constraint)):
				sys.stderr.write("! ERROR: could not find file %s\n"%(args.constraint))
				exit()
			else:
				handle=open(args.constraint,"r")
				constraint=handle.read()
				handle.close()
				constraint=sorted(re.compile("([^\s\n\r,;\(\)\[\]\"\']+)",re.M).findall(constraint))
		if(((args.constraint)and(dic[i] in constraint))or(not args.constraint)):
			if(args.verbose):
				sys.stdout.write("-- terminal %d of %d\n"%(i+1,len(dic)))
			if(args.verbose):
				sys.stdout.write("")
			combined_clades=[]
			for j in clades:
				for x in clades[j]:
					x=list(set(x)-set([i]))
					if(len(x)>1)and(combined_clades.count(x)==0):
						combined_clades+=[x]
			rank[dic[i]]=len(combined_clades)
			if(args.verbose):
				sys.stdout.write("   [%s = %d]\n"%(dic[i],len(combined_clades)))
		else:
			pass
	if(args.verbose):
		sys.stdout.write("> Terminals ranked according to the number of combined clades\nTerminal\tClades\n")
	for key in sorted(rank,key=lambda key:rank[key]):
		sys.stdout.write("%s\t%d\n"%(key,rank[key]))
	return

checkFiles()
all_trees_str=readTrees()
all_trees_list=prepTrees(all_trees_str)
if(args.root):
	all_trees_list=reRoot(all_trees_list)
clades,dic=parseTrees(all_trees_list)
rank(clades,dic)

final="""
> Done!
	NOTE: Terminals above are ranked according to the number of different clades resulting
	from the combination of all trees after the corresponding terminal is removed. The
	strict consensus will have more politomies when number of combined clades is higher.
	Terminals that result in fewer combined clades when removed are more likely to be
	considered 'wildcards' or 'rogue taxa'.
"""
if(args.verbose):
	sys.stdout.write("%s"%(final))

exit()
