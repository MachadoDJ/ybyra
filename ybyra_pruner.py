#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ybyra_pruner.py
# Read list of terminals and trees in Newick format, creating output trees if wich those terminals are pruned
# This program is part of the YBYRÁ package. Please check YBYRÁ's licence

# Import modules and libraries
import argparse,re,sys
from Bio import Phylo

# Set arguments
parser=argparse.ArgumentParser()
parser.add_argument("-tr","--tree",help="Tree file in Newick format",type=str,required=True)
parser.add_argument("-te","--terminals",help="List of terminals to be pruned out, one per line",type=str,required=True)
parser.add_argument("-o","--output",help="Prefix for output files",type=str,default="pruned_",required=False)
parser.add_argument("-v","--verbose",help="Increases output verbosity",action="store_true",default=False,required=False)
parser.add_argument("-a","--all",help="Remove all terminals from the same tree",action="store_true",default=False,required=False)
args=parser.parse_args()

# Define functions
def main():
	if(args.verbose):
		sys.stdout.write("Reading terminals\n")
	terminals=parse_te()
	if(args.verbose):
		sys.stdout.write("Parsing trees\n")
	if args.all:
		parse_tr_all(terminals)
	else:
		parse_tr(terminals)
	return

def parse_te():
	terminals=[]
	handle=open(args.terminals,"r")
	for line in handle.readlines():
		line=line.strip()
		if(line):
			terminals+=[line]
	handle.close()
	return terminals

def parse_tr(blacklisted_terminals):
	for i, terminal_to_remove in enumerate(blacklisted_terminals, 1):
		if(args.verbose):
			sys.stdout.write(f"Pruning trees for terminal {terminal_to_remove} ({i})\n")
		tree_list=[]
		trees=Phylo.parse(args.tree,"newick")
		for j, tree in enumerate(trees, 1):
			if tree.find_any(terminal_to_remove):
				tree.prune(terminal_to_remove)
				tree_list.append(tree)
		outputName=f"{args.output}{terminal_to_remove}.trees"
		Phylo.write(tree_list,outputName,"newick")
		scriptLine=f"java -jar MSdist.jar -m -i {outputName} -o msdist_{terminal_to_remove}.out\n"
		script=open(f"{args.output}script.sh","a")
		script.write(scriptLine)
		script.close()
	return

def parse_tr_all(blacklisted_terminals):
	trees = Phylo.parse(args.tree,"newick")
	tree_list = []
	for i, tree in enumerate(trees, start=1):
		for j, terminal_to_remove in enumerate(blacklisted_terminals, 1):
			if tree.find_any(terminal_to_remove):
				if(args.verbose):
					sys.stdout.write(f"Removing terminal {terminal_to_remove} ({j}) from {args.tree} (tree {i})\n")
				tree.prune(terminal_to_remove)
			tree_list.append(tree)
	outputName=f"{args.output}.trees"
	if(args.verbose):
		sys.stdout.write(f"Saving prunned tree as {args.output}.trees\n")
	Phylo.write(tree_list, outputName, "newick")
	return

# Execute functions
main()

# Quit
exit()
